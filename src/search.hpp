#include <string>
#include <random>
#include <iostream>

using std::string;
using namespace std;

int search(string text, string pattern)
{
   
    int text_size = text.size();
    int pattern_size = pattern.size();
   
    int min = -1;
    int max = pattern_size;
   
    string temp_text = "";
   
   if(pattern.size()==0) return -1;
    for(int i = 0; i < text_size; i++)
    {
        for(int j = (min + 1); j < max; j++)
        {
            temp_text = temp_text + text[j];
        }
       
        max = max+1;
        min = (max -1) - pattern_size;
       
        //cout << endl << "Pattern = " << pattern << " and text = " << temp_text << endl;
       
        if( pattern == temp_text)
        {
            return min;
        }
        temp_text = "";
    }
   
    return -1;
}
